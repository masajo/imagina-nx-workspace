export const getGreeting = () => cy.get('h1');

/**
 *
 * * EN ESTE ARCHIVO SE ESPECIFICAN FUNCIONES
 * * QUE REALICEN ACCIONES SOBRE LA APP CUANDO ESTEMOS
 * * EJECUTANDO PRUEBAS E2E. Se lleman HELPERS y se pueden
 * * reutilizar (al ser exportados)
 * * en cualquier archivo de pruebas del proyecto.
 *
 * ? De esta manera no hace falta reescribir cada vez que necesitamos
 * ? realizar un evento u obtener un elemento del DOM.
 *
 *
 * Creamos dos helpers para probar Cypress e2e
 * y obtener elementos del DOM
 * 1. Obtener un TODO de una Lista
 * 2. Obtener el botón de Añadir un TODO a la Lista
 */

export const getTodos = () => cy.get('li.todo'); // <li class="todo"></li>
export const getAddTodoButton = () => cy.get('button#add-todo'); // <button id="add-todo"></button>
