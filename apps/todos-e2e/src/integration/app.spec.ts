// import { getGreeting } from '../support/app.po';

import { getAddTodoButton, getTodos } from "../support/app.po";

describe('TodosApp', () => {

  // Antes de cada test, se navega a la raíz de la aplicación
  beforeEach(() => cy.visit('/'));

  it('Debería mostrar una lista inicial con 2 TODOS', () => {
    getTodos().should((t) => expect(t.length).equal(2));
  });

  it('Debería poder añadir un TODO a la lista', () => {
    // Contexto - Inicialmente debería haber 2 Todos en la Lista
    getTodos().should((t) => expect(t.length).equal(2));
    // Acción - Realizar CLICK sobre el botón de añadir Todo
    getAddTodoButton().click();
    // Verificación - Ahora debería haber 3 Todos en la Lista
    getTodos().should((t) => expect(t.length).equal(3));
  });

});
