import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Todo } from '@imagina/data';

@Component({
  selector: 'imagina-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'todos';
  // Nuestra lista de TODOS
  todos: Todo[] = [];

  constructor(private http: HttpClient) {}


  ngOnInit() {
    this.getAllTodos();
  }

  /**
   * Método para obtener la lista de TODOS de la aplicación de servidor
   */
  getAllTodos() {
    this.http.get<Todo[]>('/api/todos').subscribe((response) => {
      this.todos = response;
    })
  }


  /**
   * Implementamos el añadir un Todo
   * a la lista de Todos
   */
  addTodo() {

    const nuevoTodo: Todo = {
      title: `Todo ${Math.floor(Math.random()*100) + 6}`
    }

    this.http.post('/api/todos', nuevoTodo).subscribe(() => {
      this.getAllTodos();
    })
  }


}
