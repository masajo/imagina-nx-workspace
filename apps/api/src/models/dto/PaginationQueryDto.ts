import { IsOptional, IsPositive } from 'class-validator';


export class PaginationQueryDto {

  // Límite de TODOS que queremos traer
  @IsOptional()
  @IsPositive()
  limit: number;

  // Offset de TODOS que vamos a traer
  @IsOptional()
  @IsPositive()
  offset: number;
}
