import { MaxLength, IsNotEmpty, IsString  } from 'class-validator';


/**
 * Creamos un DTO (Data Transfer Object) para Crear TODOS
 * y no utilizar directamente la entidad
 **/
export class CreateTodoDto {

  @IsString()
  @IsNotEmpty()
  @MaxLength(30)
  readonly title: string

  // Aquí pondríamos más campos de haberlos
  // y sus correspondientes validaciones
  //[ ... ]


}

