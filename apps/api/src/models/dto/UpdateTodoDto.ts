import { PartialType } from '@nestjs/mapped-types';
import { CreateTodoDto } from './CreateTodoDto';

/**
 * Clase para actualizar un TODO y extiende la configuración
 * del DTO de Creación, ya que los campos son los mismos
 */
export class UpdateTodoDto extends PartialType(CreateTodoDto) {

  /**
   * En caso de que tuviera otros campos
   * además de los que ya tiene el DTO de Creación
   * los añadiríamos aquí
   * [ ... ]
   */

}
