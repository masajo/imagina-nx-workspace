// Necesitaremos de Mongoose el Document para obtener los documentos
import { Document } from 'mongoose';

// Necesitaremos de Mongoose de Nestjs el Schema, Prop, SchemaFactory
import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';


/**
 * La clase TODO está reprensentada en Mongo como un Documento
 * con un "title"
 */

@Schema()
export class Todo extends Document {
  @Prop()
  title: string;
}


// Exportamos el Esquema del Documento que se va a alojar en Mongo
export const TodoSchema = SchemaFactory.createForClass(Todo);

