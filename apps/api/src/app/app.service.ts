import { Injectable, NotFoundException } from '@nestjs/common';

// Modeo para el ejemplo sin conexión a MongoDB
// import { Todo } from '@imagina/data';

// Importaciones para conexión con Mongo:
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

// DTOS de nuestro proyecto para gestionar las peticiones al Servidor de Mongo
import { CreateTodoDto } from '../models/dto/CreateTodoDto';
import { UpdateTodoDto } from '../models/dto/UpdateTodoDto';
import { PaginationQueryDto } from '../models/dto/PaginationQueryDto';
import { Todo } from '../models/schema/Todo';
import { ITodo } from '../models/ITodo.interface';


@Injectable()
export class AppService {


  constructor(@InjectModel(Todo.name) private readonly todoModel: Model<Todo>){}


  // Lista Original de TODOS SIN CONEXIÓN A MONGO
  // todos: Todo[] = [
  //   {
  //     title: 'Todo 1'
  //   },
  //   {
  //     title: 'Todo 2'
  //   }
  // ]

  /**
   * 1. GET DATA SIN MONGO
   * Método que sirve para ofrecer la lista de TODOS
   * @returns Lista de TODOS
   */
  // getData(): Todo[] {
  //   return this.todos;
  // }

  /**
   * 2. AÑADIR TODO SIN MONGO
   * Método para añadir un nuevo Todo a la lista
   */
  // addTodo() {
  //   this.todos.push({
  //     title: `New Todo ${Math.floor(Math.random() * 1000) + 3}`
  //   })
  // }


  // Métodos que hacen uso de la conexión Mongo con Mongoose

  // 1- findAll()
  async findALL(paginationQuery: PaginationQueryDto): Promise<Todo[]> {
    // Obtenemos el límite y el offset de Paginación recibida
    // En esta línea generamos dos variables por separado (limir y offset) que coinciden
    // con las propiedades de la clase PaginationQuerDTO
    const { limit, offset } = paginationQuery;
    // Devolvemos la lista de Todos
    return await this.todoModel.find().skip(offset).limit(limit).exec();
  }


  // 2 - findOne(id)
  async findOne(id: string): Promise<Todo>{

    // Obtenemos el Todo por ID
    const todo = await this.todoModel.findById({
      _id: id
    }).exec();

    // Comprobamos que el Todo exista
    if (!todo) {
      throw new NotFoundException(`Todo: ${id} not found`);
    }

    // Devolvemos el Todo Al
    return todo;

  }

  // 3 - create()
  async create(createTodoDto: CreateTodoDto): Promise<ITodo> {

    // Definimos a partir del DTO la tarea a crear
    const newTodo = await new this.todoModel(createTodoDto);

    // Guardamos el nuevo todo a través de SAVE
    return newTodo.save();

  }


  // 4 - update()
  async update(id: string, updateTodoDto: UpdateTodoDto): Promise<ITodo> {


    // Nos aseguramos de que exista el Todo en la base de datos Mongo
    const existingTodo = await this.todoModel.findByIdAndUpdate(
      {
        _id: id
      },
      updateTodoDto
    );

    if (!existingTodo) {
      throw new NotFoundException(`Todo ${id} not found`);
    }

    return existingTodo;
  }


  // 5 - delete()
  async delete(id: string): Promise<any>{

    const deleteTodo = await this.todoModel.findByIdAndRemove(id);

    if (!deleteTodo) {
      throw new NotFoundException(`Todo ${id} not found`);
    }

    return deleteTodo;

  }


}
