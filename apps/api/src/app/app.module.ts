import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'; // Módulo para contectar con el Mongo Server
import { Todo, TodoSchema } from '../models/schema/Todo';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    // Configuración de Mongoose
    // 1. La conexión
    // 2. Los modelos y sus esquemas
    MongooseModule.forRoot('mongodb://localhost:27017/angular-todos',
      { useFindAndModify: false }
    ),
    MongooseModule.forFeature([
      {
        name: Todo.name,
        schema: TodoSchema
      }
    ])
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
