import {
  Controller, Get,
  Post, Put,
  Delete, Res,
  HttpStatus, NotFoundException,
  Body,
  Query,
  Param
} from '@nestjs/common';

import { AppService } from './app.service';

//  Importamos los DTOS de nuestro Modelo
import { CreateTodoDto } from '../models/dto/CreateTodoDto';
import { UpdateTodoDto } from '../models/dto/UpdateTodoDto';
import { PaginationQueryDto } from '../models/dto/PaginationQueryDto';


@Controller()
export class AppController {

  constructor(private readonly appService: AppService) {}

  /**
   * GET - http://localhost:3333/api/todos
   * @returns La ejecución de GetData (Lista de Todos)
   */
  @Get('todos')
  async getData(@Res() res, @Query() paginationQuery: PaginationQueryDto) {
    // Ejemplo sin Mongo:
    // return this.appService.getData();
    // Con Mongo:
    const todos = await this.appService.findALL(paginationQuery);
    // Devolvemos la Respuesta con un 200 como Status Code
    return res.status(HttpStatus.OK).json(todos);
  }


  @Get('todos/:id')
  async getTodoByID(@Res() res, @Param('id') id) {

    // Obtenemos el TODO
    const todo = await this.appService.findOne(id);

    // Si no existe le devolvemos un 404
    if (!todo) {
      throw new NotFoundException(`Todo ${id} does not exist`);
    }

    // Si existe, le devolvemos un 200 con el todo encontrado
    return res.status(HttpStatus.OK).json(todo);
  }


  /**
   * POST - http://localhost:3333/api/addTodos
   * @param res Respuesta al cliente
   * @param createTodoDto DTO de creación del TODO (BODY del POST)
   * @returns La ejecución de addTodo para añadir un Todo a la lista
   */
  @Post('todos')
  async addTodo(@Res() res, @Body() createTodoDto: CreateTodoDto) {
    // Ejemplo sin Mongo:
    // return this.appService.addTodo();
    // Con Mongo:
    try {
      const todo = await this.appService.create(createTodoDto);

      return res.status(HttpStatus.OK).json({
        message: 'Todo has been created sucessfully',
        todo
      });

    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error. Todo NOT created',
        status: 400
      });
    }
  }

  @Put('todos/:id')
  async updateTodo(@Res() res, @Body()updateTodoDto: UpdateTodoDto, @Param('id')id: string) {
    try {

      // Obtenemos el todo
      const todo = await this.appService.update(id, updateTodoDto);

      // Si no existe le devolvemos un 404
      if (!todo) {
        throw new NotFoundException(`Todo ${id} does not exist`);
      }

      // Si existe, le devolvemos un 200 con el todo encontrado
      return res.status(HttpStatus.OK).json({
        message: 'Todo updated successfuly'
      });

    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error. Todo NOT updated',
        status: 400
      });
    }
  }

  @Delete('todos/:id')
  async deleteTodo(@Res() res, @Param('id')id: string) {
    try {

      // Obtenemos el todo
      const todo = await this.appService.delete(id);

      // Si no existe le devolvemos un 404
      if (!todo) {
        throw new NotFoundException(`Todo ${id} does not exist`);
      }

      // Si existe, le devolvemos un 200 con el todo encontrado
      return res.status(HttpStatus.OK).json({
        message: 'Todo deleted successfuly'
      });

    } catch (error) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error. Todo NOT deleted',
        status: 400
      });
    }
  }


}
