export function data(): string {
  return 'data';
}

/**
 * Exportamos la Interface de Todo
 * para que hagan uso tanto la Aplicación de
 * Angular como la de Nest de ella
 */
export interface Todo {
  title: string
}
