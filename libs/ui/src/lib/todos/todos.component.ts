import { Component, Input } from '@angular/core';
import { Todo } from '@imagina/data';

@Component({
  selector: 'imagina-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent {

  @Input() todos: Todo[]

  // constructor() {}

  // ngOnInit(): void {}

}
